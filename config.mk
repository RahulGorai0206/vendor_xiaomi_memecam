PRODUCT_SOONG_NAMESPACES += \
    vendor/xiaomi/memecam

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/memecam/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/memecam/proprietary/system/lib,$(TARGET_COPY_OUT_SYSTEM)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/memecam/proprietary/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/memecam/proprietary/system/priv-app/ANXCamera/lib/arm,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/memecam/proprietary/system/priv-app/ANXCamera/lib/arm64,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm64)

PRODUCT_PACKAGES += \
    ANXCamera \
    ANXExtraPhoto \
    ANXScanner \
    anxres \
    framework-ext-res \
    miui \
    miuisystem \
    anxframework \
    miuiframework

PRODUCT_PRODUCT_PROPERTIES += \
    ro.miui.notch=1

# Build
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
