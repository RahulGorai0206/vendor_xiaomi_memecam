# ANXCamera
## About branches :
- Use 
main
 branch only.
## Getting Started :
### Cloning :
- Clone this repo in packages/apps/memecam in your working directory by :
```
git clone https://gitlab.com/RahulGorai0206/vendor_xiaomi_memecam vendor/xiaomi/memecam
```
### Changes Required :
- Add [this](https://github.com/RahulGorai0206/device_xiaomi_sdm845-common/commit/6fb58a58cf3ea72e48cc2fcfe92a999200a8d8a3) commit.
- Done, continue building your ROM as you do normally.

### Credits :
- [ANX Group](https://camera.aeonax.com/)
- [KARTHIK LAL](https://github.com/karthik558/vendor_aeonax_ANXCamera)
- [Aryan Sinha](https://github.com/begonia-crdroid/android_packages_apps_ANXCamera)
- [ME ALSO](https://github.com/RahulGorai0206)
